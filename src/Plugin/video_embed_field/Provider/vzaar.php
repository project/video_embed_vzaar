<?php

namespace Drupal\video_embed_vzaar\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A Vzaar provider plugin for video embed field.
 *
 * @VideoEmbedProvider(
 *   id = "vzaar",
 *   title = @Translation("vzaar")
 * )
 */
class vzaar extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    // check if video ID is numberic (vzaar), else assume it is in the new dacast format

    // vzaar
    if (is_numeric($this->getVideoId())) {
      $iframe = [
        '#type' => 'video_embed_iframe',
        '#provider' => 'vzaar',
        '#url' => sprintf('https://view.vzaar.com/%s/player', $this->getVideoId()),
        '#query' => [],
        '#attributes' => [
          'id' => sprintf('vzvd-%s', $this->getVideoId()),
          'name' => sprintf('vzvd-%s', $this->getVideoId()),
          'width' => $width,
          'height' => $height,
          'frameborder' => '0',
          'allowfullscreen' => 'allowfullscreen',
        ],
      ];  
    }

    // dacast
    else {
      $iframe = [
        '#type' => 'video_embed_iframe',
        '#provider' => 'vzaar',
        '#url' => sprintf('%s', $this->getVideoId()),
        '#query' => [],
        '#attributes' => [
          'width' => $width,
          'height' => $height,
          'frameborder' => '0',
          'allowfullscreen' => 'allowfullscreen',
          'scrolling' => 'no',
          'allow' => 'autoplay'
        ],
      ];  
    }

    return $iframe;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    // check if video ID is numberic (vzaar), else assume it is in the new dacast format
    
    // vzaar
    if (is_numeric($this->getVideoId())) {
      $url = 'https://view.vzaar.com/%s/thumb';
    }

    // dacast
    else {
      $url = '%s';
    }

    return sprintf($url, $this->getVideoId());
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    if (strpos($input, 'view.vzaar.com') !== false) {
      // e.g. https://view.vzaar.com/848450/download
      $id= explode('/',parse_url($input,PHP_URL_PATH))[1];
      return isset($id) ? $id : false;
    }
    else if (strpos($input, 'iframe.dacast.com') !== false) {
      // DaCast Share Link, eg https://iframe.dacast.com/vod/d8c2dd79-6bd7-1b71-5d80-97608fec9df3/12cf941b-6e6e-40f4-5e2f-00c4034e8123 - we can use this in the iframe
      return ($input);
    }
    
    return false;
  }
}
