An integration for vzaar videos (now owned by Dacast) into Video Embed Field. Videos hosted at vzaar.com or dacast.com can be added into the video embed field using the video link.

Just add a video embed link from the vzaar / dacast platform to the field - eg: https://view.vzaar.com/848450/download or https://iframe.dacast.com/vod/d8c2dd79-6bd7-1b71-5d80-97608fec9df3/12cf941b-6e6e-40f4-5e2f-00c4034e8123 
